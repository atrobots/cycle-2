/*
 *File: bullet.h
 *Author: AT Robots team
 */

#ifndef BULLET_H
#define BULLET_H
#include <string>
#include <math.h>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "robot.h"


//const float PI = 3.1415926535;
//const int arenaX = 0;
//const int arenaY = 0;
//const int arenaW = 640;
//const int arenaH = 480;
//Note: the actual arena variables are pending an
//update from Vlad

class Bullet {

	private:
		bool valid; //flag
		int damage; 
		int speed;
		int length;
		int width;
		float direction; //a range from 0-360
    /*
    *Coordinates of Bullet: note, the point (x1, y1)
    *reffers to the point of origination of the bullet,
    *the center of the robot---- the point (x2, y2) reffers
    *to the point that extends the bullet length, outward
    *from the center of the robots
    *therefore, for rendering with, the call should
    * actually be thickLineRGBA(renderer, bullet.x2, bullet.y2,
    *bullet.x1, bullet.y1, R, G, B, A)
    */
		float x;
		float y;
		//float x2;
		//float y2;
		int R;
		int G;
		int B;
    	int A;

	public:
		Bullet(float X, float Y, float direction);
		bool is_valid();
		float getX();
		float getY();
		//float getX2();
		//float getY2();
    float getDirection();
		int getR();
		int getG();
		int getB();
		int getA();
		void set_valid(bool v);
		void setX(float X);
		void setY(float Y);
		//void setX2(float x);
		//void setY2(float y);
		int getSpeed();
		int getDamage();
		int getLength();
		int getWidth();

		//motion related functions
		bool keep_moving(int aX, int aY, int aW, int aH, Robot* rArray[]);
		void move();
		float nextX();
		float nextY();
		//float nextX2();
		//float nextY2();

		//direction related functions
		float check_degree(float degree);
		float deg_to_rad(float degree);
		//float rad_to_deg(float radian);
		//int getQuadrant(float degree);

		//collision detection
		bool collide_wall(int aX, int aY, int aW, int aH);
		bool collide_robot(Robot* rArray[]);
                float get_distance(Robot robot);

		//render the bullet in its present position
		void renderBullet(SDL_Renderer* renderer);

		void despawn();
		~Bullet();
};

#endif /*BULLET_H*/
