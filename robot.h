#ifndef ROBOT_H
#define ROBOT_H

#include <string>
#include <SDL2/SDL.h>
#include <vector>
#include <math.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>
#include "miniat.h"
#include "gun.h"

class Robot {
    private:
    int health = 100;
    float targetAngle;
    float offsetAngle = 15.00;
    float targetSpeed;
    float turnSpeed;
    int colorR;
    int colorG;
    int colorB;
    std::string name;
    bool collideRobot;
    //Weapon weapon;
    //Scanner scanner;
  
    public:
    Robot (float, float, float, int, int, int, FILE*);
    void renderRobot (SDL_Renderer*);
    void addWeapon ();
    //void addScanner (Scanner);
    void setHealth (int);
    void setTurnAngle (float);
    void setOffsetAngle (float);
    void setThrottle (float);
    float turn (float, float, float);
    float drive (float, float);
    void checks ();
    float getX();
    float getY();
    float getCurrentAngle();
    ~Robot ();
    bool isHit;
    float x;
    float y;
    float currentAngle;
	float currentSpeed;
	Gun gun;
	miniat *m;
};
/*class Weapon {
public:
    Weapon();
    virtual ~Weapon() = 0;
    void rotate(int);
    void rotateWith(float);
    void aim(float);
    virtual void draw() = 0;
    virtual bool shoot(float) = 0;
  
};*/


const int arenaX = 0;
const int arenaY = 0;
const int arenaW = 640;
const int arenaH = 480;
//Note: the actual arena variables are pending an
//update from Vlad

#endif /* ROBOT_H */
