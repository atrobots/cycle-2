/* 
 * File:   main.cpp
 * Author: AT_Robots Team
 *
 * Created on February 23, 2017, 5:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <SDL2/SDL.h>
#include <random>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_config.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>

#include "miniat.h"
#include "peripherals.h"
#include "ports.h"
#include "robot.h"
#include "gun.h"
#include "bullet.h"

using namespace std;
int mainStart (int argc, char *argv[]);

/*
 * 
 */
Robot* rArray[1] = {};
std::vector<Bullet*> bullets;

int main(int argc, char *argv[]){
	TRY {
		mainStart(argc, argv);
	}
		CATCHALL {
		miniat_dump_error();
	}
	FINALLY {}
	ETRY;

	return EXIT_SUCCESS;
}

int mainStart(int argc, char *argv[]) {
    SDL_Window* window = NULL;
    SDL_Surface* surface = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Init(SDL_INIT_VIDEO);

    if(TTF_Init()<0){
        std::cout << "Error" << TTF_GetError() << std::endl;
	}
    window = SDL_CreateWindow("AT Robots", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
    surface = SDL_GetWindowSurface(window);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> x(1, 300);
    std::uniform_int_distribution<int> y(1, 200);
    std::uniform_int_distribution<int> angle(1, 360);
    std::uniform_int_distribution<int> color(0x00, 0xFF);

	
    
    //creates the array of robots and the robots
    int i = 0;
    int size = 1;
    

    for (i = 0; i < size; i++){
	FILE* rName = fopen(argv[i], "r+b");
        rArray[i] = new Robot ((x(rng)), (y(rng)), (angle(rng)), (color(rng)), (color(rng)), (color(rng)), rName);
        rArray[i]->addWeapon();
        rArray[i]->renderRobot (renderer);
    }
    
    //forever
    while(true) {
        TTF_Font* Sans = TTF_OpenFont("batmfa.ttf",11);
        SDL_Color White = {255, 255, 255};
        SDL_Surface* surfaceMessage1 = TTF_RenderText_Solid(Sans, "Robot1", White);
        SDL_Surface* surfaceMessage2 = TTF_RenderText_Solid(Sans, "Robot2", White);
        SDL_Surface* surfaceMessage3 = TTF_RenderText_Solid(Sans, "Robot3", White);
        SDL_Surface* surfaceMessage4 = TTF_RenderText_Solid(Sans, "Robot4", White);
        SDL_Texture* Message1 = SDL_CreateTextureFromSurface(renderer, surfaceMessage1);
        SDL_Texture* Message2 = SDL_CreateTextureFromSurface(renderer, surfaceMessage2);
        SDL_Texture* Message3 = SDL_CreateTextureFromSurface(renderer, surfaceMessage3);
        SDL_Texture* Message4 = SDL_CreateTextureFromSurface(renderer, surfaceMessage4);
        SDL_Rect Message1_rect;
        SDL_Rect Message2_rect;
        SDL_Rect Message3_rect;
        SDL_Rect Message4_rect;
        if(Sans == nullptr){
            std::cout << "Failed to load font: " << SDL_GetError() << std::endl;
            return false;
        }
        Message1_rect.x = 550;
        Message1_rect.y = 50;
        Message1_rect.w = 50;
        Message1_rect.h = 25;

        Message2_rect.x = 550;
        Message2_rect.y = 115;
        Message2_rect.w = 50;
        Message2_rect.h = 25;

        Message3_rect.x = 550;
        Message3_rect.y = 175;
        Message3_rect.w = 50;
        Message3_rect.h = 25;

        Message4_rect.x = 550;
        Message4_rect.y = 240;
        Message4_rect.w = 50;
        Message4_rect.h = 25;
        
        SDL_Event e;
        if(SDL_PollEvent(&e)) {
            if(e.type == SDL_QUIT || e.key.keysym.sym == SDLK_q) {
                break;
            }
        }
	rArray[0]->turn(rArray[0]->getCurrentAngle(), 0.00, 15.00);
	rArray[0]->drive(rArray[0]->currentSpeed, 50.00);
	bullets.push_back(rArray[0]->gun.shoot(rArray[0]->currentAngle,rArray[0]->x,rArray[0]->y));
        //runs checks each cycle
        for (int c = 0; c < 30; c++){
            for (i = size; i >= 0; i--){
                rArray[i]->checks ();
            }
        }
        //updates the robot position and renders each frame
        for (i = size; i >= 0; i--){
            rArray[i]->renderRobot (renderer);
        }
        SDL_RenderClear(renderer);
        
        boxRGBA(renderer, 15, 0, 0, 480, 1, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 0, 0, 15, 1, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 465, 0, 480, 0x00, 0x00, 0x00, 0xff);
	boxRGBA(renderer, 640, 0, 540, 480, 0x00, 0x00, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 23, 600, 43, 0xff, 0x00, 0x00, 0xff);
	//Health box
	boxRGBA(renderer, 635, 43, 610, 23, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 80, 600, 100, 0x00, 0xff, 0x00, 0xff);
	//Health box
	boxRGBA(renderer, 635, 100, 610, 80, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 145, 600, 165, 0x00, 0x00, 0xff, 0xff);
	//Health box
	boxRGBA(renderer, 635, 166, 610, 144, 0x00, 0xff, 0x00, 0xff);

	//Color
	boxRGBA(renderer, 555, 210, 600, 230, 0xff, 0xff, 0x00, 0xff);
        //Health box
        boxRGBA(renderer, 635, 230, 610, 210, 0x00, 0xff, 0x00, 0xff);

	SDL_SetRenderDrawColor(renderer, 191, 191, 191, SDL_ALPHA_OPAQUE);
        SDL_RenderCopy(renderer, Message1, NULL, &Message1_rect);
	SDL_RenderCopy(renderer, Message2, NULL, &Message2_rect);
	SDL_RenderCopy(renderer, Message3, NULL, &Message3_rect);
	SDL_RenderCopy(renderer, Message4, NULL, &Message4_rect);
    
        SDL_RenderDrawLine(renderer, 640, 16, 540, 16);
        SDL_RenderDrawLine(renderer, 640, 70, 540, 70);
        SDL_RenderDrawLine(renderer, 640, 135, 540, 135);
        SDL_RenderDrawLine(renderer, 640, 198, 540, 198);
        SDL_RenderDrawLine(renderer, 640, 263, 540, 263);
        SDL_RenderPresent(renderer);
        
        
        //???????
        SDL_SetRenderDrawColor(renderer, 191, 191, 191, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);
        
        
    }
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();

    return 0;

}

double get_distance(Robot* robot, Bullet* bullet) {\
    //gets the distance between the robot's center point and the leading point of the bullet
	/*
	if (robot == NULL) {
		return -1;
	}
	*/
	double distance;
	double bX = (double)bullet -> getX();
	double bY = (double)bullet -> getY();
	double rX = (double)robot -> getX();
	double rY = (double)robot -> getY();
	double xDif = rX - bX;
	double yDif = rY - bY;
	double xDifsq = xDif * xDif;
	double yDifsq = yDif * yDif;
	distance = sqrt(xDifsq+yDifsq);

	return distance;
}

void collide_robot(){
	double distance;
	double range = 15;
	for (int i = 0; i <= 1; i++) {
            for (int j = 0; j <= bullets.size(); j++){
                distance = get_distance(rArray[i], bullets.at(j));
		if (distance <= range) {
			rArray[i] -> isHit = true;
                        bullets.erase(j);
                }
            }
        }
}