/*
 *File: bullet.cpp
 *Author: AT Robots team
 */

#include "bullet.h"
#include <math.h>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>

//Constructor and Destructor deffinition
Bullet::Bullet(float X, float Y, float degree) {
	if (check_degree(degree) >= 0) {
		this -> valid = true;
		this -> x = X;
		this -> y = Y;
		this -> direction = check_degree(degree);
		this -> length = 10;
		this -> width = 3;
		this -> damage = 1;
		this -> speed = 1;
		this -> R = 255;
		this -> G = 0;
		this -> B = 0;
		this -> A = 100;
	} else {
		//else everything is initialized to whatever its
		//default value is / NULL
		//therefore, the 'valid' flag is used to check for an
		//a useable bullet
		this -> valid = false;
	}
};

Bullet::~Bullet(){}

//member function definitons
//-----------------------------------------------------------

//Degree related functions
//-----------------------------------------------------------
/**
	checks to see if the direction is valid (0<= x <360)
	@param: the degree to check
	@return: either the original degree, the original%360, or
	-1 if original was negative
*/
float  Bullet::check_degree(float degree) {
	if (degree >= 0 && degree < 360) {
		return degree;
	}
	if (degree < 0) {
		return -1;
	}
	float newDegree = fmod(degree, 360);
	return newDegree;
}

/**
	converts a degree to radians
	@param: a float degree
	@return: a float radian
*/
float Bullet::deg_to_rad(float degree) {
	float good_degree = check_degree(degree);
	if (good_degree > -1) {
		float a = good_degree * M_PI;
		float b = a/180;
		return b;
	}
	return -1;
}

//Getter functions
//----------------------------------------------------------------
bool Bullet::is_valid() {
	return valid;
}
float Bullet::getX() {
	return x;
}
float Bullet::getY() {
	return y;
}
float Bullet::getDirection() {
	return direction;
}
int Bullet::getR() {
	return R;
}
int Bullet::getG() {
	return G;
}
int Bullet::getB() {
	return B;
}
int Bullet::getA() {
	return A;
}
int Bullet::getSpeed() {
	return speed;
}
int Bullet::getDamage() {
	return damage;
}
int Bullet::getLength() {
	return length;
}
int Bullet::getWidth() {
	return width;
}

//Setter functions
//-------------------------------------------------------------
void Bullet::set_valid(bool v) {
	valid = v;
}
void Bullet::setX(float X) {
	x = X;
}
void Bullet::setY(float Y) {
	y = Y;
}

//Collision detection
//------------------------------------------------------------
/**
	determines if the bullet has collided with the edge of
	the screen--Note: the arena rectangle is conceptualy
	defined by rect(x,y,width, height) and the arena rect
	is rect(x!=0, y!=0, w, h)
	NOTE: playable dimensions  of arena are
	(topleft x,y)x(botom right x,y)
	(15,15)x(540,465)
	@param: the height and width of the screen
	@return: bool true if the bullet hits the edges
*/
bool Bullet::collide_wall() {
	float X = getX();
	float Y = getY();
	if (X <= 15 || X >= 540) {
		return true;
	}
	if (Y <= 15 || Y >= 465) {
		return true;
	}
	return false;
}



//ROBOT COLLISION MOVED TO MAIN
/**
	determines if the bullet has collided with any robot in
	the arena, by looping through an std::vector of the robots
	Note: the bullet collides with a robot if the bullet(x2,y2)
	is within a radius of 15 pixels from the robot(x,y) center
	@param: a std::vector<> of Robot objects
	@return: bool true if the bullet hit a robot
*/
/*
bool Bullet::collide_robot(const std::vector<Robot> & robots){
	double distance;
	double range = 15;
	for (auto & r : robots) {
		distance = get_distance(r);
		if (distance <= range) {
			r.isHit = true;
			//currently some problems working with Robot.cpp
			return true;
		}
	}
	return false;
}
*/
/**
	gets the distance between the leading point of the bullet(x,y)
	and the center of the robot
	@param: a Robot object
	@return: the float distnce between the two, or -1 if the robot
	object is null
*/
/*
float Bullet::get_distance(Robot robot) {
	//if (robot == NULL) {
	//	return -1;
	//}
	double distance;
	double bX = (double)getX();
	double bY = (double)getY();
	double rX = (double)robot.getX();
	double rY = (double)robot.getY();
	double xDif = rX - bX;
	double yDif = rY - bY;
	double xDifsq = xDif * xDif;
	double yDifsq = yDif * yDif;
	distance = sqrt(xDifsq+yDifsq);

	return distance;
}
*/

//Motion functions
//-------------------------------------------------------------
/**
	finds the next x position based on direction
	@param: void
	@return: the float position of x1 next
*/
float Bullet::nextX() {
	float rad = deg_to_rad(getDirection());
	float X = getX();
	float s = (float)getSpeed();
	return (X+(s * (cos(rad))));
}

/**
	finds the next y position based on direction
	@param: void
	@return: the float position of y1 next
*/
float Bullet::nextY() {
	float rad = deg_to_rad(getDirection());
	float Y = getY();
	float s = (float)getSpeed();
	return (Y + (s * (sin(rad))));
}

/**
	updates the coordinates of the bullet (x,y)
	@param: none
	@return: none
*/
void Bullet::move() {
	float nX = nextX();
	float nY = nextY();
	x = nX;
	y = nY;
}

//NOT IN USE
/**
	keeps track of whether the bullet should continue moving,
	or stop moving if it has collided with something
	@param: int dimensions of the arena, and a vector  of robots
	@return: bool true if no collisions, false if some Collision
*/
/*
bool Bullet::keep_moving(const std::vector<Robot> & robots) {
	if (collide_wall()) {
		return false;
	}
	if (collide_robot(robots)) {
		return false;
	}
	return true;
}
*/

/**
	renders bullet in its current position
	*Coordinates of Bullet: note, the point (x, y)
	*reffers to the point of origination of the bullet,
	*the end of the gun---- the point (trailingX, trailingY) reffers
	*to a point which is only rendered, and is not used un other calculations
	*therefore, for rendering with, the call should
	* actually be thickLineRGBA(renderer, bullet.x, bullet.y, R, G, B, A)
	@param: the renderer object
	@return: void
*/
void Bullet::renderBullet(SDL_Renderer* renderer) {
	float rad = deg_to_rad(getDirection());
	float l = (float)getLength();
	float X = getX();
	float Y = getY();
	float trailingX = X-(l * (cos(rad)));
	float trailingY = Y-(l * (sin(rad)));
	thickLineRGBA(renderer, X, Y, trailingX, trailingY, getWidth(), getR(), getG(), getB(), getA());
}

/**
	gets rid of a bullet when it collides with something
	by setting its 'valid' flag to false
	@param: none
	@return: none
*/
void Bullet::despawn() {
	set_valid(false);
}
