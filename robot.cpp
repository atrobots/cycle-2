/* 
 * File:   Robot.cpp
 * Author: AT_Robots Team
 * 
 * Created on February 23, 2017, 5:17 PM
 */

#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <inttypes.h>

#define PI 3.14159265358979323846
#define OFFSETTHROTTLE 1

#include "gun.h"
#include "robot.h"
//#include "miniat.h"
//#include "peripherals.h"
#inlcude "ports.h"



Robot::Robot(float x, float y, float angle, int colorR, int colorG, int colorB, FILE* rName) {
    this -> x = x;
    this -> y = y;
    this -> currentAngle = angle;
    this -> colorR = colorR;
    this -> colorG = colorG;
    this -> colorB = colorB;
	this -> m = miniat_new(rName, NULL);
    //addScanner();
}

float Robot::getX(){
    return this->x;
}
float Robot::getY(){
    return this->y;
}

float Robot::getCurrentAngle(){
    return this->currentAngle;
}

void Robot::addWeapon(){
    //If the robot only has one weapon then it can be a var. No add func.
    //calls weapon class to instantiate an object on this robot
}
void Robot::renderRobot (SDL_Renderer* renderer) {
    //renders the robot at current x, y, and angle with specific colors
    //renders red filled triangle
    filledTrigonRGBA(renderer,
					 x+20*cos(currentAngle*PI/180),
					 y-20*sin(currentAngle*PI/180), 
					 x-20*cos(currentAngle*PI/180)-10*cos((currentAngle+90)*PI/180), 
					 y+20*sin(currentAngle*PI/180)+10*sin((currentAngle+90)*PI/180), 
					 x-20*cos(currentAngle*PI/180)+10*cos((currentAngle+90)*PI/180), 
					 y+20*sin(currentAngle*PI/180)-10*sin((currentAngle+90)*PI/180),
					 colorR, colorG, colorB, 0xff);
    

	thickLineRGBA(renderer,
				  x-5*cos(currentAngle*PI/180),
				  y+5*sin(currentAngle*PI/180), 
				  x-5*cos(currentAngle*PI/180)+25*cos(currentAngle*PI/180), 
				  y+5*sin(currentAngle*PI/180)+25*sin(currentAngle*PI/180),
				  4,
				  0x00, 0x00, 0x00, 0xff);
   
}
//WILL BE ADDED LATER
/*void Robot::addScanner (Scanner s) {
    //calls scanner class to instantiate an object on this robot
}*/

void Robot::setHealth (int h) {
    //sets health value
    this->health = health - h;
}

void Robot::setTurnAngle (float angle) {
    //sets target turn angle value + counterclockwise - clockwise
    this->targetAngle = angle;
}

void Robot::setOffsetAngle (float angleChange) {
    //sets offset turn angle value + counterclockwise - clockwise
    this->offsetAngle = angleChange;
}

void Robot::setThrottle (float target) {
    //sets target speed value -75 to 100
	this->targetSpeed = target;
}

float turn (float current, float target, float offset) {
    //turns the robot clockwise or counterclockwise
    if (abs (target - current) > offset) {
        if (current < target){
            current += offset;
        }
        else{
            current -= offset;
        }
    }
    else{
        current = target;
    }
    return current;
}

float drive (float current, float target) {
    //drives the robot forward or backward
    if (abs (target - current) > OFFSETTHROTTLE) {
        if (current < target){
            current += OFFSETTHROTTLE;
        }
        else{
            current -= OFFSETTHROTTLE;
        }
    }
    else{
        current = target;
    }
    return current;
}

void Robot::checks() {
	if (collideRobot) {
		//stop the robot from moving
	}
	if (isHit) {
		//take damage
		//WHAT IF HIT BY 2 BULLETS AT SAME TIME?!?!?!?
	}
	if (health <= 0) {
		//blow up the robotealth to be negative
	}
	//weapon.tick();
	if (currentAngle != targetAngle) {
		turn(currentAngle, targetAngle, offsetAngle);
	}
	if (currentSpeed != targetSpeed) {
		drive(currentSpeed, targetSpeed);

	}
}

Robot::~Robot() {
    
}
