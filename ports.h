#ifndef __PORTS_H__
#define __PORTS_H__

#include <stdbool.h>

#include "miniat.h"

extern bool doubles;
extern bool draw_target;

extern void ports_cleanup();
extern void ports_clock(miniat *m);

#endif /* __PORTS_H__ */
