#ifndef __PERIPHERALS_H__
#define __PERIPHERALS_H__

#include <stdbool.h>

#include "miniat.h"


//extern int keyb_update_counter;
extern bool quit;
extern char target_char;
extern char current_char;

extern void peripherals_cleanup();
extern void peripherals_clock(miniat *m);


#endif /*  __PERIPHERALS_H__ */
